"""add monitor event column

Revision ID: 6fed83ef0817
Revises: 5d26eb9225d0
Create Date: 2020-05-10 17:27:21.267122

"""
from alembic import op
import sqlalchemy as sa
import importlib.util
import os.path


# revision identifiers, used by Alembic.
revision = '6fed83ef0817'
down_revision = '5d26eb9225d0'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('draft', sa.Column('monitor_event_id', sa.Integer))

def downgrade():
    pth = os.path.join(os.path.dirname(__file__), '596635b8e0e6_create_drafting_table.py')
    spec = importlib.util.spec_from_file_location("upstream", pth)
    upstream = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(upstream)

    with op.batch_alter_table("draft") as batch_op:
        batch_op.drop_column('monitor_event_id')
    op.execute(upstream.DRAFT_TRIGGER)
