"""create drafting table

Revision ID: 596635b8e0e6
Revises: 4d5e3ecd597f
Create Date: 2020-05-09 13:25:12.134390

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '596635b8e0e6'
down_revision = '4d5e3ecd597f'
branch_labels = None
depends_on = None


DRAFT_TRIGGER = """
    CREATE TRIGGER draft_updated_at
        AFTER UPDATE ON draft
        FOR EACH ROW
        WHEN NEW.updated_at <= OLD.updated_at
    BEGIN
        UPDATE draft SET updated_at=CURRENT_TIMESTAMP WHERE id=OLD.id;
    END"""

ANIMAL_TRIGGER = """
    CREATE TRIGGER animal_updated_at
        AFTER UPDATE ON animal
        FOR EACH ROW
        WHEN NEW.updated_at <= OLD.updated_at
    BEGIN
        UPDATE animal SET updated_at=CURRENT_TIMESTAMP WHERE id=OLD.id;
    END"""

def upgrade():
    op.create_table(
        'draft',
        sa.Column('id', sa.Integer, nullable=False, primary_key=True, autoincrement=True),
        sa.Column('animal_id', sa.INTEGER, sa.ForeignKey('animal.id', onupdate='CASCADE', ondelete='CASCADE')),
        sa.Column('type', sa.String),
        sa.Column('triggered_at', sa.TIMESTAMP),
        sa.Column('removed_at', sa.TIMESTAMP),
        sa.Column('created_at', sa.TIMESTAMP, server_default=sa.func.now()),
        sa.Column('updated_at', sa.TIMESTAMP, server_default=sa.func.now())
    )

    op.execute(DRAFT_TRIGGER)
    op.execute(ANIMAL_TRIGGER)

    op.create_table(
        'seen_event',
        sa.Column('animal_id', sa.INTEGER, sa.ForeignKey('animal.id', onupdate='CASCADE', ondelete='CASCADE')),
        sa.Column('seen_at', sa.TIMESTAMP))

def downgrade():
    op.execute("DROP TRIGGER draft_updated_at")
    op.drop_table('draft')
    op.execute("DROP TRIGGER animal_updated_at")
    op.drop_table("seen_event")
