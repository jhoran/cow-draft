"""create animal table

Revision ID: 4d5e3ecd597f
Revises:
Create Date: 2020-05-04 11:25:17.178423

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4d5e3ecd597f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'animal',
        sa.Column('id', sa.Integer, nullable=False, primary_key=True, autoincrement=True),
        sa.Column('brand', sa.Integer),
        sa.Column('drafter_rfid', sa.INTEGER),
        sa.Column('last_seen_at', sa.TIMESTAMP),
        sa.Column('created_at', sa.TIMESTAMP, server_default=sa.func.now()),
        sa.Column('updated_at', sa.TIMESTAMP, server_default=sa.func.now())
    )


def downgrade():
    op.drop_table('animal')
