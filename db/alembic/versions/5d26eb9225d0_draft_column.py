"""add direction column to draft

Revision ID: 5d26eb9225d0
Revises: 596635b8e0e6
Create Date: 2020-05-09 18:09:40.243020

"""
from alembic import op
import sqlalchemy as sa
import importlib.util
import os.path

# revision identifiers, used by Alembic.
revision = '5d26eb9225d0'
down_revision = '596635b8e0e6'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('draft',
        sa.Column('direction', sa.String))
    op.add_column('animal',
        sa.Column('drafter_id', sa.Integer))


def downgrade():
    pth = os.path.join(os.path.dirname(__file__), '596635b8e0e6_create_drafting_table.py')
    spec = importlib.util.spec_from_file_location("upstream", pth)
    upstream = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(upstream)

    with op.batch_alter_table("draft") as batch_op:
        batch_op.drop_column('direction')

    op.execute(upstream.DRAFT_TRIGGER)

    with op.batch_alter_table("animal") as batch_op:
        batch_op.drop_column("drafter_id")
    op.execute(upstream.ANIMAL_TRIGGER)

