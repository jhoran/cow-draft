import yaml
import telegram

class Notify:
    def __init__(self):
        with open('config/secrets.yaml') as fin:
            secrets = yaml.load(fin,  Loader=yaml.SafeLoader).get('telegram')

        self._bot = telegram.Bot(token=secrets['token'])


    def send(self, message):
        # self._bot.send_message(90354292, 'hello',
        #     reply_markup=telegram.InlineKeyboardMarkup([[
        #         telegram.InlineKeyboardButton('Greetings', callback_data='ans_a'),
        #         telegram.InlineKeyboardButton('Salutations', callback_data='ans_b')
        #     ]]))
        self._bot.send_message(90354292, message)

    def test_recv(self):
        upds = self._bot.get_updates()
        for m in upds:
            print(m)

if __name__ == "__main__":
    n = Notify()
    n.test_recv()