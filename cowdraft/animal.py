"""Module related to animals"""
from __future__ import annotations
import logging
from datetime import timedelta

log = logging.getLogger(__name__) # pylint:disable=invalid-name
class Animal:
    """Class representing an animal in the database"""
    _db = None

    @classmethod
    async def with_id(cls, row_id) -> Animal:

        assert cls._db, 'No database connection'

        cursor = await cls._db.execute("""
            SELECT id AS row_id, brand, drafter_rfid, last_seen_at, drafter_id
            FROM animal
            WHERE id = ?
        """, (row_id,))
        row = await cursor.fetchone()
        if row:
            return Animal(**row)
        return None

    @classmethod
    async def with_drafter_rfid(cls, drafter_rfid: int) -> Animal:
        assert cls._db, 'No database connection'

        cursor = await cls._db.execute("""
            SELECT id AS row_id, brand, drafter_rfid, last_seen_at, drafter_id
            FROM animal
            WHERE drafter_rfid = ?
        """, (drafter_rfid,))
        row = await cursor.fetchone()

        if row:
            return Animal(**row)
        return None

    @classmethod
    async def with_brand(cls, brand: int) -> Animal:
        assert cls._db, 'No database connection'
        cursor = await cls._db.execute("""
            SELECT id AS row_id, brand, drafter_rfid, last_seen_at, drafter_id
            FROM animal
            WHERE brand = ?
            ORDER BY updated_at DESC
        """, (brand,))
        row = await cursor.fetchone()

        if row:
            return Animal(**row)
        return None

    @classmethod
    async def create(cls, drafter_rfid: int, brand: int, drafter_id: int):
        assert cls._db, 'No database connection'

        await cls._db.execute("""
            INSERT INTO animal(brand, drafter_rfid, drafter_id)
            VALUES (?, ?, ?)
        """, (brand, drafter_rfid, drafter_id))
        await cls._db.commit()

        cursor = await cls._db.execute("""
            SELECT id AS row_id
            FROM animal
            WHERE drafter_rfid = ?
        """, (drafter_rfid,))
        row = await cursor.fetchone()

        self = Animal(row['row_id'],
            brand=brand,
            drafter_rfid=drafter_rfid,
            drafter_id=drafter_id)

        log.info("Added new %s", self)
        return self

    def __init__(self, row_id,
        drafter_rfid=None,
        brand=None,
        last_seen_at=None,
        drafter_id=None
    ):
        self.id = row_id # pylint: disable=invalid-name
        self.drafter_rfid = drafter_rfid
        self.brand = brand
        self.last_seen_at = last_seen_at
        self.drafter_id = drafter_id

    def __repr__(self):
        if self.brand is not None:
            return f'Animal(brand={self.brand})'
        return f'Animal(drfid={self.drafter_rfid})'

    async def set_brand(self, brand):
        await self._db.execute("""
            UPDATE animal
            SET brand = ?
            WHERE id = ?
        """, (brand, self.id))
        await self._db.commit()

        old_brand = self.brand
        self.brand = brand

        log.info('Updated brand for %s, was %s', self, old_brand)

    async def set_last_seen(self, timestamp):
        if (self.last_seen_at is not None
            and (timestamp - self.last_seen_at) < timedelta(seconds=20)
        ):
            logging.debug('%s returns too soon', self)
            return

        await self._db.execute("""
            UPDATE animal
            SET last_seen_at = ?
            WHERE id = ?
        """, (timestamp, self.id))
        if self.last_seen_at:
            await self._db.execute("""
                INSERT INTO seen_event(animal_id, seen_at)
                VALUES (?, ?)
            """, (self.id, timestamp))
        await self._db.commit()

        self.last_seen_at = timestamp
        log.info('Set %s seen at %s', self, timestamp)

    async def update_drafter_id(self, drafter_id):
        await self._db.execute("""
            UPDATE animal
            SET drafter_id = ?
            WHERE id = ?
        """, (drafter_id, self.id))
        await self._db.commit()
        self.drafter_id = drafter_id

        log.info('Updated internal drafter id for %s', self)
