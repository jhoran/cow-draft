"""Module for interacting with drafter"""
import json
import logging
import asyncio
from datetime import datetime, timedelta
import websockets

from cowdraft.animal import Animal

log = logging.getLogger(__name__) # pylint: disable=invalid-name

class Drafter:
    """ Class for drafting gate """

    @classmethod
    async def create(cls, *args, **kwargs):
        """ Create a connection to the drafting gate and return """
        websocket = await websockets.connect(f"ws://{kwargs['address']}:{kwargs['port']}")
        await websocket.send(json.dumps({"type": "query", "query": "herd"}))

        self = Drafter(*args, websocket=websocket, **kwargs)

        return self

    def __init__(self, address, port, database, websocket):
        self._addr = f'{address}:{port}'
        self._db = database.conn
        self._ws = websocket

    async def listen(self):
        """Listen for messages from the drafter"""
        while True:
            message = json.loads(await self._ws.recv())

            if message['data_type'] == 'herd':
                await self.update_herd(message['data']['animals'])
            elif message['data_type'] == 'animal':
                await self.update_animal(message['data']['animal'])
            elif message['data_type'] == 'sensors':
                for sensor in message['data']['sensors']:
                    log.debug(
                        'Drafter sensor %s status %s',
                        sensor['name'], sensor['status'])
            else:
                log.info(
                    'Unhandled "%s" message; %s...',
                    message['data_type'],
                    json.dumps(message['data'])[:300])

    async def update_herd(self, herd):
        """Update all animals in the database from the drafter"""
        tasks = []
        for animal in herd:
            task = self.update_animal(animal['animal'])
            tasks.append(task)

        await asyncio.gather(*tasks)


    async def update_animal(self, entry):

        rfid = int(entry['rfid'].replace(' ', ''))
        try:
            brand = int(entry['animalid'])
        except ValueError:
            brand = None

        animal = await Animal.with_drafter_rfid(rfid)
        # New animal
        if not animal:
            animal = await Animal.create(
                drafter_rfid=rfid,
                brand=brand,
                drafter_id=entry['rowid'])

        tasks = []

        if animal.brand != brand:
            tasks.append(animal.set_brand(brand))

        last_seen_at = (datetime.now() - timedelta(
            seconds=entry['timesincelastseen']))

        tasks.append(animal.set_last_seen(last_seen_at))

        divert = entry['divert']
        divert = (
            None if divert == 0 else
            'left' if divert == 1 else
            'right' if divert == 2 else
            'unknown')

        tasks.append(self.set_drafter_divert(animal, divert))

        if divert and entry['timesincelastseen'] < 5:
            tasks.append(self.set_diversion_triggered(animal, last_seen_at))

        if animal.drafter_id != entry['rowid']:
            tasks.append(animal.update_drafter_id(entry['rowid']))

        asyncio.gather(*tasks)


    async def get_drafter_divert(self, animal, current_only=False):
        query = """
            SELECT type, direction
            FROM draft
            WHERE animal_id = ?
        """

        if current_only:
            query += " AND removed_at IS NULL"
        query += " ORDER BY created_at DESC"

        results = await self._db.execute(query, (animal.id, ))
        if current_only:
            return await results.fetchone()
        else:
            return await results.fetchall()

    async def auto_divert(self, animal, direction):
        try:
            await self.set_drafter_divert(animal, direction, 'pending')
        except ValueError:
            return

        num_divert = 1 if direction == 'left' else 2 if direction == 'right' else 0

        await self._ws.send(json.dumps({
            "type":"update",
            "update":"animal",
            "rowid": animal.drafter_id,
            "animalid": f"{animal.brand:04}" if animal.brand is not None else "????",
            "divert": num_divert}))


    async def set_drafter_divert(self, animal, direction, diversion_type='manual'):
        cur_diversion = await self.get_drafter_divert(animal, current_only=True)
        cur_direction = cur_diversion and cur_diversion['direction'] or None
        cur_diversion_type = cur_diversion and cur_diversion['type'] or None

        if cur_diversion_type == 'manual' and diversion_type == 'pending':
            raise ValueError("Can't automatically divert already manually diverted animal")

        if direction == cur_direction and cur_diversion_type != 'pending':
            return

        if direction and not cur_direction:
            await self._db.execute("""
                INSERT INTO draft (animal_id, type, direction)
                VALUES (?, ?, ?)
            """, (animal.id, diversion_type, direction))
            log.info('%s will be diverted %s, %s', animal, direction, diversion_type)

        elif direction == cur_direction and cur_diversion_type == 'pending':
            await self._db.execute("""
                UPDATE draft
                SET type = 'automatic'
                WHERE removed_at IS NULL
                AND animal_id = ?
            """, (animal.id,))
            log.info("Acknowledged draft request for %s", animal)

        elif direction and cur_direction:
            await self._db.execute("""
                UPDATE draft
                SET direction = ?
                WHERE animal_id = ?
                AND removed_at IS NULL
            """, (direction, animal.id))

        elif cur_direction and not direction:
            await self._db.execute("""
                UPDATE draft
                SET removed_at = ?
                WHERE removed_at IS NULL
                AND animal_id = ?
            """, (datetime.now(), animal.id))
            log.info('Diversion removed for %s', animal)

        else:
            log.error('%s wants to go %s, has %s', animal, direction, cur_direction)

        await self._db.commit()

    async def set_diversion_triggered(self, animal, timestamp):
        await self._db.execute("""
            UPDATE draft
            SET triggered_at = ?
            WHERE removed_at IS NULL
            AND animal_id = ?
        """, (timestamp, animal.id))
        await self._db.commit()
        log.info("%s has been diverted", animal)

        asyncio.create_task(
            self.remove_diversion(animal))

    async def remove_diversion(self, animal, timeout=600):
        await asyncio.sleep(timeout)

        cur_diversion = await self.get_drafter_divert(animal, current_only=True)
        if not cur_diversion:
            return

        if cur_diversion['type'] != 'automatic':
            return

        await self._ws.send(json.dumps({
            "type":"update",
            "update":"animal",
            "rowid": animal.drafter_id,
            "animalid": f"{animal.brand:04}" if animal.brand is not None else "????",
            "divert": 0}))
        log.info("Automatic divert for %s has been removed", animal)


    async def stop(self):
        await self._ws.close()
        log.info('Closed connection to drafter')
