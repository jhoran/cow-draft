import json
import yaml
import asyncio
import aiohttp
import logging
import random
from datetime import datetime, timedelta, time

from cowdraft.animal import Animal

log = logging.getLogger(__name__)
class Monitor:

    def __init__(self, address, port=80, database=None, session_cookies=None, drafter=None):
        self._addr = address
        if port != 80:
            self._addr += f':{port}'

        self._listen_success = datetime.now()
        self._db = database.conn
        self.drafter = drafter
        self._session_cookies = session_cookies

    @classmethod
    async def create(cls, *args, **kwargs):

        addr = kwargs['address']
        port = kwargs.get('port', 80)
        if port != 80:
            addr = f'{addr}:{port}'

        with open('config/secrets.yaml') as fin:
            secrets = yaml.load(fin, Loader=yaml.SafeLoader)['monitor']
        username = secrets['username']
        password = secrets['password']

        async with aiohttp.ClientSession() as session:
            resp = await session.post(
                f'http://{addr}/login/LoginPage.web?action=1',
                data={'select_username': username, 'input_password': password},
                allow_redirects=False)

        if resp.status != 302:
            raise ValueError(f'Expected redirect, got {resp.status}')
        location = resp.headers.get('Location')
        if location != f'http://{addr}/':
            raise ValueError(f'Expected redirect to http://{addr}/ got {location}')

        cookies = {}
        for cookie in resp.headers.getall('Set-Cookie'):
            key, val = cookie.split(';')[0].split('=')
            cookies[key] = val

        log.info("Logged in succesfully")


        self = Monitor(*args, session_cookies=cookies, **kwargs)
        return self

    async def stop(self):
        pass

    async def listen(self):
        while True:
            try:
                await asyncio.gather(
                    self.check_attention('acute'),
                    self.check_attention('heat'))
                self._listen_success = datetime.now()
            except (asyncio.TimeoutError, json.JSONDecodeError) as err:
                last_success = datetime.now() - self._listen_success

                if last_success > timedelta(minutes=30):
                    if isinstance(err, json.JSONDecodeError):
                        log.exception("JSON decode exception causing restart")
                    raise
                log.info("Connection issue, last succeeded %d seconds ago", last_success.seconds)

                await asyncio.sleep(20)
                continue

            await asyncio.sleep(120)

    async def check_attention(self, query_type):

        if query_type == "acute":
            method = "health.AnimalAcuteAttentionsPage.getHealthSummaries"
        elif query_type == "heat":
            now = datetime.now()
            if now.time() < time(5) or now.time() > time(11):
                log.info("Skipping heat checks until the morning")
                return
            if now.month not in [4, 5, 6, 7]:
                #Only for April to July
                log.info("Skipping heat checks out of season")
                return
            method = "activity.HeatDetectionAttentionPage.getAttentions"
        else:
            raise ValueError("Unknown method %s" % query_type)

        async with aiohttp.ClientSession(
            cookies=self._session_cookies,
            timeout=aiohttp.ClientTimeout(total=20)
        ) as session:
            req = await session.post(
                f'http://{self._addr}/ajax',
                data=json.dumps({
                    "jsonrpc": "2.0",
                    "method": method,
                    "id": random.randint(1, 9999999999),
                    "params": []}))
            resp = await req.text()

        if not resp:
            return

        attentions = [json.loads(r)['result'] for r in resp.split('\n\n') if r]

        log.info('Received %d attention messages', len(attentions))
        tasks = []
        for attention in attentions:
            tasks.append(self.handle_attention(attention))

        await asyncio.gather(*tasks)


    async def handle_attention(self, event):
        if event['attention']['checked']:
            return

        if event['attention']['type'] == 4:
            logging.info("Skipping suspicious event for %s", event['animal']['number'])
            return

        event_id = event['attention']['id']

        check_exists = await self._db.execute("""
            SELECT id
            FROM draft
            WHERE monitor_event_id = ?
            LIMIT 1
        """, (event_id, ))
        exists = await check_exists.fetchone()
        if exists:
            log.debug("Event %d already handled, ignoring", event_id)
            return

        brand = event['animal']['number']
        animal = await Animal.with_brand(brand)

        if not animal:
            log.info("Couldn't add event for %d as animal not on drafter", brand)
            return

        await self.drafter.auto_divert(animal, 'right')
        await self._db.execute("""
            UPDATE draft
            SET monitor_event_id = ?
            WHERE removed_at IS NULL
            AND animal_id = ?
        """, (event_id, animal.id))
        await self._db.commit()
        log.warning("%s has been added to draft list", animal)
