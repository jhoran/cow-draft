
import logging
import sqlite3
import aiosqlite

from cowdraft.animal import Animal

log = logging.getLogger(__name__) # pylint: disable=invalid-name

class Database:


    def __init__(self, address):
        self._addr = address
        self.conn = None

    # pylint: disable=protected-access
    @classmethod
    async def create(cls, *args, **kwargs):
        self = Database(*args, **kwargs)

        self.conn = await aiosqlite.connect(
            self._addr,
            detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
        self.conn.row_factory = lambda cursor, row: {
            k[0]: v for k, v in zip(cursor.description, row)}

        Animal._db = self.conn

        return self


    async def stop(self):
        await self.conn.close()
