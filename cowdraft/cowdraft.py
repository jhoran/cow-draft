import os
import logging
import yaml
import argparse
import sentry_sdk
import asyncio

from cowdraft.database import Database
from cowdraft.drafter import Drafter
from cowdraft.monitor import Monitor
# from cowdraft.notify import Notify

log = logging.getLogger(__name__) # pylint: disable=invalid-name

async def main():
    conf = config()

    sentry_init()

    database = await Database.create(**conf['database'])

    drafter = Drafter.create(**conf['drafter'], database=database)
    monitor = Monitor.create(**conf['monitor'], database=database)

    try:
        drafter = await drafter
        monitor = await monitor

        monitor.drafter = drafter

        await asyncio.gather(
            drafter.listen(),
            monitor.listen())
    except asyncio.CancelledError:
        pass
    finally:
        if isinstance(drafter, Drafter):
            await drafter.stop()

        if isinstance(monitor, Monitor):
            await monitor.stop()

        await database.stop()




def sentry_init():
    if os.environ['USER'] != 'cow-draft':
        return

    try:
        with open('.git/refs/heads/master') as fin:
            version = fin.read()
    except IOError:
        log.info("Couldn't read git sha to send to sentry")


    sentry_sdk.init(
        "https://d12443cf259746459da759ad60274599@"
        "o277724.ingest.sentry.io/5213594",
        release=version
    )

    log.debug('Initialized sentry')

def handle_exception(loop, context):
    # context["message"] will always be there; but context["exception"] may not
    msg = context.get("exception", context["message"])
    log.error("Caught exception: %s", msg)
    log.info("Shutting down...")
    asyncio.create_task(shutdown(loop))


async def shutdown(_, signal=None):
    """Cleanup tasks tied to the service's shutdown."""
    if signal:
        log.info("Received exit signal %s...", signal.name)
    log.info("Closing database connections")

def config():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='config/cowdraft.yaml')
    parser.add_argument('--drafter-port', type=int)

    args = parser.parse_args()
    with open(args.config) as fin:
        config_file = yaml.load(fin.read(), Loader=yaml.SafeLoader)

    if args.drafter_port:
        config_file['drafter']['port'] = args.drafter_port

    return config_file
