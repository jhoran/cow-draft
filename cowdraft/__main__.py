
import logging
import sys
import asyncio
import os

import cowdraft.cowdraft


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO if os.environ['USER'] == 'cow-draft' else logging.DEBUG)

    loop = asyncio.get_event_loop()
    task = loop.create_task(cowdraft.cowdraft.main())
    try:
        loop.run_until_complete(task)
    except KeyboardInterrupt:
        logging.warning('Keyboard Shutdown')
    finally:
        logging.info('running finally')
        task.cancel()
        loop.run_until_complete(task)
