# Cow Draft

Simple project to interface Pearsons system with ODonnell's draft gate.

### Installation
```shell
groupadd -g 995 srv
useradd -d /srv/cow-draft -g srv -s /sbin/nologin -N -u 5000 cow-draft

sudo -u cow-draft -i
ssh-keygen -t ed25519
cat .ssh/id_ed25519.pub # Copy to gitlab as deploy key

ssh-keygen -t ed25519 -f ~/gitlab-key
cat <<- EOF | tr -d '\n' | sed 's/$/\n/g' >> .ssh/authorized_keys
no-agent-forwarding,
no-port-forwarding,
no-x11-forwarding,
no-user-rc,
no-pty,
command="git pull" $(cat ~/gitlab-key.pub)
EOF
cat gitlab-key | base64 # Copy to gitlab as ssh key
rm gitlab-key gitlab-key.pub

cd ~
git init
git remote add origin git@gitlab.com:jhoran/cow-draft.git
git fetch
git checkout -t origin/master
git config --local core.hooksPath config/hooks/

exit
cd /srv/cow-draft
sudo make install

```
