
cwd := $(shell pwd)

install:
	cat config/cow-draft.service \
		| sed  "s%\$${PWD}%${cwd}%g" \
		> /etc/systemd/system/cow-draft.service
	/bin/systemctl daemon-reload
