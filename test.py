#!/usr/bin/env python3
import logging
import os
from datetime import datetime

def main():
    logging.info('Hello %s, the time was %s', os.environ['USER'], datetime.now())

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    main()